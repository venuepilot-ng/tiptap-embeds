import { Node } from "@tiptap/core";

export interface IframeOptions {
  allowFullscreen: boolean;
  HTMLAttributes: {
    [key: string]: any;
  };
}

export enum EmbedType {
  YOUTUBE = "youtube",
  VIMEO = "vimeo",
  SOUNDCLOUD = "soundcloud",
  SPOTIFY = "spotify",
  BANDCAMP = "bandcamp",
  CUSTOM = "custom",
}

declare module "@tiptap/core" {
  interface Commands<ReturnType> {
    tiptapEmbed: {
      setEmbed: (embedCode: string, type?: EmbedType) => ReturnType;
      setEmbedAlign: (position: string) => ReturnType;
      unsetEmbedAlign: () => ReturnType;
    };
  }
}

function extractAttributes(iframeTag: string): Record<string, string> {
  const attributes: Record<string, string> = {};
  const regex = /(\w+)\s*=\s*(['"])(.*?)\2/g;
  let match: RegExpExecArray | null;

  while ((match = regex.exec(iframeTag))) {
    const attributeName = match[1];
    const attributeValue = match[3];
    attributes[attributeName] = attributeValue;
  }
  return attributes;
}

export default Node.create<IframeOptions>({
  name: "tiptapEmbed",
  group: "block",
  atom: true,
  addAttributes() {
    return {
      src: {
        default: null,
      },
      frameborder: {
        default: 0,
      },
      allow: {
        default: null,
      },
      allowfullscreen: {
        default: true,
      },
      name: {
        default: null,
      },
      referrerpolicy: {
        default: null,
      },
      loading: {
        default: null,
      },
      sandbox: {
        default: null,
      },
      seamless: {
        default: null,
      },
      scrolling: {
        default: null,
      },
      srcdoc: {
        default: null,
      },
      height: {
        default: null,
      },
      width: {
        default: null,
      },
      style: {
        default: "",
      },
      metaPosition: {
        default: "flex-start",
      },
      metaType: {
        default: EmbedType.YOUTUBE,
      },
    };
  },
  parseHTML() {
    return [
      {
        tag: "iframe-embed",
      },
    ];
  },
  renderHTML({ HTMLAttributes }) {
    return ["iframe-embed", HTMLAttributes];
  },
  addCommands() {
    return {
      setEmbed:
        (embedCode: string, type?: EmbedType) =>
        ({ tr, dispatch }) => {
          const { selection } = tr;
          const attributes = extractAttributes(embedCode);
          if (type) attributes.metaType = type;
          const nodeAttrs = { ...attributes };
          const node = this.type.create(nodeAttrs);

          if (dispatch) {
            tr.replaceRangeWith(selection.from, selection.to, node);
          }

          return true;
        },
      setEmbedAlign:
        (position: string) =>
        ({ commands }) => {
          commands.updateAttributes(this.type, { metaPosition: position });
          return true;
        },
      unsetEmbedAlign:
        () =>
        ({ commands }) => {
          return commands.resetAttributes(this.type, "metaPosition");
        },
    };
  },
});
