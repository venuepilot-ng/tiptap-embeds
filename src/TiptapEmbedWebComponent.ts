import { EmbedType } from "./embed";
const iframeEmbedTypes = [
  EmbedType.YOUTUBE,
  EmbedType.VIMEO,
  EmbedType.SOUNDCLOUD,
  EmbedType.SPOTIFY,
  EmbedType.SOUNDCLOUD,
  EmbedType.BANDCAMP,
  EmbedType.CUSTOM,
];

class TiptapEmbedWebComponent extends HTMLElement {
  connectedCallback() {
    const metaType = this.getAttribute("metaType") as EmbedType;
    if (!iframeEmbedTypes.includes(metaType)) {
      return;
    }
    const iframe = document.createElement("iframe");

    const attributes = Array.from(this.attributes);
    attributes.forEach((attribute) => {
      iframe.setAttribute(attribute.name, attribute.value);
    });

    // Centering embed
    const div = document.createElement("div");
    const position = this.getAttribute("metaPosition");
    let positionValue = "flex-start";
    if (position) {
      positionValue = position;
    }
    div.setAttribute(
      "style",
      `display: flex; justify-content: ${positionValue};`
    );

    div.appendChild(iframe);
    this.appendChild(div);
  }
}

customElements.define("iframe-embed", TiptapEmbedWebComponent);

export default TiptapEmbedWebComponent;
