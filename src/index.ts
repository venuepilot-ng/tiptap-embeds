import TiptapEmbed, { EmbedType } from "./embed";
import TiptapEmbedWebComponent from "./TiptapEmbedWebComponent";
export { TiptapEmbed, TiptapEmbedWebComponent,  EmbedType };

export default TiptapEmbed;
