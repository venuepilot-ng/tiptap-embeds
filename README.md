# TiptapEmbed

Tiptap embed extension. You can use any embed that use `Iframe` HTML element with it.
It works by creating custom Web component tag and passing all attributes defined on `IFrame`.
This enables safely storing output is DB as prevents injection.

## Installation

```bash
yarn tiptap-embed
```

## Notes

At the moment this package is configured to be used directly without build.
In order to use build replace in config:

```
  "main": "src/index.ts",
  "module": "src/index.ts",
```

with

```
  "main": "dist/index.cjs.js",
  "module": "dist/index.js",
  "types": "dist/index.d.ts",
```
